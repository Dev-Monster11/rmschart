import RMSGraph from "./RMSGraph";
import "./App.css";
import { useState, useEffect, intervalRef } from "react";
import GraphSettings from "./GraphSettings";
import { Button } from "@mui/material";

function App() {
    const generateXY = (xa, xb, ya, yb) => {
        var xValue = [],
            yValue = [],
            colorValue = [];
        for (var i = xa; i <= xb; i += Math.floor(Math.random() * 10) / 10) {
            xValue.push(i);
            yValue.push(ya + Math.floor(Math.random() * (yb - ya)));

            colorValue.push(Math.floor(Math.random() * 3));
        }
        return [xValue, yValue, colorValue];
    };
    const values = generateXY(0, 100, 0, 100);
    const [chartData, setChartData] = useState({
        title: "RMS Chart",
        showTitle: true,
        depthRange: [0, 10],
        nrmRange: [0, 10],
        inputData: {
            normRMS: 5,
            depth: values[0],
            rms: values[1],
            color_type: values[2],
        },
    });
    const updateInputData = (newData) => {
        setChartData((prevChartData) => ({
            ...prevChartData,
            inputData: newData,
        }));
    };
    const fetchAndUpdateData = async () => {
        try {
            const response = await fetch("./RMSTestingData.json");
            if (!response.ok) {
                throw new Error(
                    "Failed to fetch data from RMSTestingData.json"
                );
            }
            const data = await response.json();
            const { RMS, Depth, ColorType } = data;
            updateInputData({
                normRMS: 1,
                depth: [],
                rms: [],
                color_type: [],
            });

            // Helper function to simulate a delay
            const sleep = (milliseconds) => {
                return new Promise((resolve) =>
                    setTimeout(resolve, milliseconds)
                );
            };

            // Update inputData iteratively with a delay of 300 milliseconds
            for (let i = 1; i < Depth.length; i++) {
                await sleep(300);
                setChartData((prevChartData) => ({
                    ...prevChartData,
                    inputData: {
                        normRMS: 1,
                        depth: Depth.slice(0, i + 1),
                        rms: RMS.slice(0, i + 1),
                        color_type: ColorType.slice(0, i + 1),
                    },
                }));
            }
        } catch (error) {
            console.error(error);
        }
    };
    useEffect(() => {
        // console.log(chartData.inputData);
    }, [chartData.inputData]);

    const handleButtonClick = () => {
        fetchAndUpdateData();
    };
    // const randomize = () => {
    //     var i = Math.floor(Math.random() * 10);
    //     const nrmRange = Math.floor(Math.random() * 100);
    //     const newValue = generateXY(0, i, 0, nrmRange);

    //     setChartData({
    //         depthRange: [0, i],
    //         nrmRange: [0, nrmRange],
    //         inputData: {
    //             normRMS: Math.floor(Math.random() * 5),
    //             depth: newValue[0],
    //             rms: newValue[1],
    //             color_type: newValue[2],
    //         },
    //     });
    // };
    const [width, setWidth] = useState(500);
    const [height, setHeight] = useState(300);
    const [depthLabel, setDepthLabel] = useState("Depth [mm]");
    const [nrmsLabel, setNRMSLabel] = useState("NRMS");
    const [depthDisplay, setDepthDisplay] = useState(true);
    const [nrmsDisplay, setNrmsDisplay] = useState(true);
    const [gridDisplay, setGridDisplay] = useState("ON");
    const [chartDirection, setChartDirection] = useState("vertical");
    const [isDarkMode, setIsDarkMode] = useState(false);

    const handleToggleTheme = (display) => {
        setIsDarkMode(display);
    };

    const theme = {
        barColors: isDarkMode
            ? ["white", "darkblue", "#333333", "darkred"]
            : ["black", "blue", "#cccccc", "red"],
        backgroundColor: isDarkMode ? "#333333" : "#ffffff",
    };
    const handleTitleChange = (newTitle) => {
        setChartData((prevChartData) => ({
            ...prevChartData,
            title: newTitle,
        }));
    };
    const handleTitleVisibilityToggle = () => {
        setChartData((prevChartData) => ({
            ...prevChartData,
            showTitle: !prevChartData.showTitle,
        }));
    };
    const handleDepthRangeChange = (newDepthRange) => {
        setChartData((prevChartData) => ({
            ...prevChartData,
            depthRange: newDepthRange,
        }));
    };

    const handleNrmRangeChange = (newNrmRange) => {
        setChartData((prevChartData) => ({
            ...prevChartData,
            nrmRange: newNrmRange,
        }));
    };
    const handleWidthChange = (newWidth) => {
        setWidth(newWidth);
    };
    const handleHeightChange = (newWidth) => {
        setHeight(newWidth);
    };
    const handleDepthLabelChange = (newLabel) => {
        console.log(newLabel)
        setDepthLabel(newLabel);
    };
    const handleNRMSLabelChange = (newLabel) => {
        setNRMSLabel(newLabel);
    };    
    const handleDepthLabelDisplay = (display) => {
        console.log(display);
        setDepthDisplay(display);
    };
    const handleNRMSLabelDisplay = (display) => {
        setNrmsDisplay(display);
    };
    const handleGridDisplayChange = (newGridDisplay) => {
        setGridDisplay(newGridDisplay);
    };
    const handleGraphDirection = (newDirection) => {
        setChartDirection(newDirection);
    };
    return (
        <>
            <GraphSettings
                isDarkMode={isDarkMode}
                title={chartData.title}
                width={width}
                height={height}
                depthLabel={depthLabel}
                nrmsLabel={nrmsLabel}
                titleDisplay={chartData.showTitle}
                depthDisplay={depthDisplay}
                nrmsDisplay={nrmsDisplay}
                gridDisplay={gridDisplay}
                chartDirection={chartDirection}
                depthRange={chartData.depthRange}
                nrmsRange={chartData.nrmRange}
                onSetDarkMode={handleToggleTheme}
                onChangeTitle={handleTitleChange}
                onDisplayTitle={handleTitleVisibilityToggle}
                onChangeDepthRange={handleDepthRangeChange}
                onChangeNrmsRange={handleNrmRangeChange}
                onSetGraphDirection={handleGraphDirection}
                onChangeWidth={handleWidthChange}
                onChangeHeight={handleHeightChange}
                onChangeDepthLabel={handleDepthLabelChange}
                onChangeNRMSLabel={handleNRMSLabelChange}
                onDisplayDepth={handleDepthLabelDisplay}
                onDisplayNrms={handleNRMSLabelDisplay}
                onDisplayGrid={handleGridDisplayChange}
            />
            <Button
                variant="contained"
                color="primary"
                onClick={handleButtonClick}
            >
                Update Data
            </Button>
            <div
                style={{
                    display: "flex",
                    justifyContent: "space-around",
                    padding: "50px",
                }}
            >
                <p>Title: {chartData.title}</p>{" "}
                <p>Show Title: {chartData.showTitle.toString()}</p>
                <p>Depth Range: {chartData.depthRange}</p>
                <p>NRMS Range: {chartData.nrmRange}</p>
                <p style={{ backgroundColor: theme.backgroundColor }}>
                    Bar colors:{" "}
                    <span style={{ color: theme.barColors[0] }}>
                        Normal Bar
                    </span>
                    ,{" "}
                    <span style={{ color: theme.barColors[1] }}>
                        Normalization Bar
                    </span>
                    ,{" "}
                    <span style={{ color: theme.barColors[2] }}>
                        Ignored Bar
                    </span>
                    ,{" "}
                    <span style={{ color: theme.barColors[3] }}>
                        Highlighted Bar
                    </span>
                </p>
            </div>
            <div
                style={{
                    display: "flex",
                    justifyContent: "space-around",
                    padding: "50px",
                }}
            >
                <p>Graph Grid: {gridDisplay.toString()}</p>
                <p>Display Depth Label: {depthDisplay.toString()}</p>
                <p>Display NRMS Label: {nrmsDisplay.toString()}</p>
                <p>Graph Direction: {chartDirection}</p>
                <div>
                    <p>
                        Depth Data: Shortened Depth Data:{" "}
                        {chartData.inputData.depth
                            ? JSON.stringify(
                                  chartData.inputData.depth.slice(0, 10)
                              ) +
                              (chartData.inputData.depth.length > 10
                                  ? "..."
                                  : "")
                            : ""}{" "}
                    </p>
                    <p>
                        Shortened RMS Data:{" "}
                        {chartData.inputData.rms
                            ? JSON.stringify(
                                  chartData.inputData.rms.slice(0, 10)
                              ) +
                              (chartData.inputData.rms.length > 10 ? "..." : "")
                            : ""}
                    </p>
                    <p>
                        Shortened Color-Type Data:{" "}
                        {chartData.inputData.color_type
                            ? JSON.stringify(
                                  chartData.inputData.color_type.slice(0, 10)
                              ) +
                              (chartData.inputData.color_type.length > 10
                                  ? "..."
                                  : "")
                            : ""}
                    </p>
                </div>
            </div>
            <RMSGraph
                title={chartData.title}
                showTitle = {chartData.showTitle}
                width={width}
                height={height}
                depthAxisLabel={depthLabel}
                showDepthAxisLabel={depthDisplay}
                NRMSAxisLabel={nrmsLabel}
                showNRMSAxisLabel={nrmsDisplay}
                // axisDirection='horizontal'
                axisDirection={chartDirection}
                depthRange={chartData.depthRange}
                nrmsRange={chartData.nrmRange}
                grid={gridDisplay}
                inputData={{
                    normRMS: 10,
                    depth: chartData.inputData.depth,
                    rms: chartData.inputData.rms,
                    color_type: chartData.inputData.color_type,
                }}
                //regular, normal, ignore, high
                barColors={theme.barColors}
                backgroundColor={theme.backgroundColor}
            />
            {/* <RMSGraph
                title="RMS Chart"
                width="1000"
                height="500"
                depthAxisLabel="Depth [mm]"
                showDepthAxisLabel={true}
                NRMSAxisLabel="NRMS"
                showNRMSAxisLabel={true}
                axisDirection="horizontal"
                //axisDirection='vertical'
                depthRange={chartData.depthRange}
                nrmsRange={chartData.nrmRange}
                grid="ON"
                inputData={{
                    normRMS: 10,
                    depth: chartData.inputData.depth,
                    rms: chartData.inputData.rms,
                    color_type: chartData.inputData.color_type,
                }}
                //regular, normal, ignore, high
                barColors={["black", "blue", "#cccccc", "red"]}
                backgroundColor="#ffffff"
            ></RMSGraph> */}
        </>
    );
}

export default App;
