import { Box, FormControlLabel, Slider, Switch } from "@mui/material";
import React, { useState } from "react";
import "./GraphSettings.css";
import { styled } from "@mui/material/styles";

const GraphSettings = ({
    onChangeTitle,
    onChangeWidth,
    onChangeHeight,
    onChangeDepthLabel,
    onChangeNRMSLabel,
    onDisplayTitle,
    onDisplayDepth,
    onDisplayNrms,
    onDisplayGrid,
    onSetGraphDirection,
    onChangeDepthRange,
    onChangeNrmsRange,
    onSetDarkMode,
    isDarkMode,
    title,
    width,
    height,
    depthLabel,
    nrmsLabel,
    titleDisplay,
    depthDisplay,
    nrmsDisplay,
    gridDisplay,
    chartDirection,
    depthRange,
    nrmsRange,
}) => {
    const [graphTitle, setGraphTitle] = useState(title);
    const [showTitle, setShowTitle] = useState(titleDisplay);
    const [graphWidth, setGraphWidth] = useState(width);
    const [graphHeight, setGraphHeight] = useState(height);
    const [depthAxisLabel, setDepthAxisLabel] = useState(depthLabel);
    const [nrmsAxisLabel, setnrmsAxisLabel] = useState(nrmsLabel);
    const [displayDepth, setDisplayDepth] = useState(depthDisplay);
    const [displayNrms, setDisplayNrms] = useState(nrmsDisplay);
    const [depthAxisRange, setDepthAxisRange] = useState(depthRange);
    const [nrmsAxisRange, setNrmsAxisRange] = useState(nrmsRange);
    const [gridGraphDisplay, setGridGraphDisplay] = useState(gridDisplay);
    const [graphDirection, setGraphDirection] = useState(chartDirection);
    const [darkMode, setDarkMode] = useState(false);

    const handleToggleTheme = (event) => {
        const newTheme = event.target.checked;
        setDarkMode(newTheme);
        onSetDarkMode(newTheme);
    };

    const handleTitleChange = (event) => {
        const newTitle = event.target.value;
        setGraphTitle(newTitle);
        onChangeTitle(newTitle);
    };
    const handleTitleDisplay = (event) => {
        const display = event.target.checked;
        setShowTitle(display);
        onDisplayTitle(display);
    };
    const handleWidthChange = (event, newValue) => {
        const newWidth = parseInt(newValue, 10); // Parse the value as a number
        setGraphWidth(newWidth);
        onChangeWidth(newWidth);
        // console.log(newWidth);
    };
    const handleHeightChange = (event, newValue) => {
        const newHeight = parseInt(newValue, 10); // Parse the value as a number
        setGraphHeight(newHeight);
        onChangeHeight(newHeight);
        // console.log(newHeight);
    };
    const handleDepthLabelChange = (event) => {
        const newDepthLabel = event.target.value;
        setDepthAxisLabel(newDepthLabel);
        onChangeDepthLabel(newDepthLabel);
    };
    const handleNRMSLabelChange = (event) => {
        const newNRMSLabel = event.target.value;
        setnrmsAxisLabel(newNRMSLabel);
        onChangeNRMSLabel(newNRMSLabel);
    };    
    const handleDepthLabelDisplay = (event) => {
        const display = event.target.checked;
        setDisplayDepth(display);
        onDisplayDepth(display);
    };
    const handleNrmsLabelDisplay = (event) => {
        const display = event.target.checked;
        setDisplayNrms(display);
        onDisplayNrms(display);
    };
    const handleGridDisplayChange = (event) => {
        const display = event.target.checked ? "ON" : "OFF";
        setGridGraphDisplay(display);
        onDisplayGrid(display);
    };
    const handleGraphDirectionChange = (event) => {
        const newDirection = event.target.checked ? "Horizontal" : "Vertical";
        setGraphDirection(newDirection);
        onSetGraphDirection(newDirection);
    };
    const handleDepthRangeChange = (event, newRange) => {
        setDepthAxisRange(newRange);
        onChangeDepthRange(newRange);
    };
    const handleNrmsRangeChange = (event, newRange) => {
        setNrmsAxisRange(newRange);
        onChangeNrmsRange(newRange);
    };
    const valuetext = (value) => {
        return `${value}`;
    };
    function generateMarks(min, max, segments) {
        const range = max - min;
        const segmentSize = range / segments;

        const marks = [];

        for (let i = 0; i <= segments; i++) {
            const value = min + segmentSize * i;
            marks.push({ value, label: String(value) });
        }

        return marks;
    }
    const widthSliderMarks = generateMarks(0, 2000, 10);
    const heightSliderMarks = generateMarks(0, 1000, 10);
    const depthRangeMarks = generateMarks(0, 40, 8);
    const nrmsRangeMarks = generateMarks(0, 40, 8);
    return (
        <>
            <FormControlLabel
                control={
                    <Switch
                        sx={{ m: 1 }}
                        checked={isDarkMode}
                        onChange={handleToggleTheme}
                    />
                }
                label="Change Graph Colors"
            />

            <div
                className="flex-box"
                style={{ display: "flex", marginBottom: "20px" }}
            >
                <div id="title-change">
                    <label>
                        Title :{" "}
                        <input
                            type="text"
                            value={graphTitle}
                            onChange={handleTitleChange}
                        />
                    </label>
                </div>
                <Box
                    style={{
                        marginLeft: "20px",
                        display: "flex",
                        flexDirection: "column",
                    }}
                >
                    <FormControlLabel
                        style={{ marginLeft: "10px" }}
                        control={
                            <Switch
                                checked={showTitle}
                                onChange={handleTitleDisplay}
                            />
                        }
                        label="Display Graph Title"
                    />
                    <FormControlLabel
                        style={{ marginLeft: "10px" }}
                        control={
                            <Switch
                                checked={displayDepth}
                                onChange={handleDepthLabelDisplay}
                                // defaultChecked
                            />
                        }
                        label="Display Depth Label"
                    />
                    <FormControlLabel
                        style={{ marginLeft: "10px" }}
                        control={
                            <Switch
                                checked={displayNrms}
                                onChange={handleNrmsLabelDisplay}
                                // defaultChecked
                            />
                        }
                        label="Display NRMS Label"
                    />
                    <FormControlLabel
                        style={{ marginLeft: "10px" }}
                        control={
                            <Switch
                                checked={gridGraphDisplay === "ON"}
                                onChange={handleGridDisplayChange}
                            />
                        }
                        label={"Display graph Grid"}
                    />
                    <FormControlLabel
                        style={{ marginLeft: "10px" }}
                        control={
                            <Switch
                                checked={graphDirection === "Horizontal"}
                                onChange={handleGraphDirectionChange}
                            />
                        }
                        label={graphDirection ? "Horizontal" : "Vertical"}
                    />
                </Box>
                <Box
                    style={{
                        marginLeft: "20px",
                        display: "flex",
                        flexDirection: "column",
                    }}
                >
                    <div id="depth-label-change">
                        <label>
                            Depth Label :{" "}
                            <input
                                type="text"
                                value={depthAxisLabel}
                                onChange={handleDepthLabelChange}
                            />
                        </label>
                    </div>
                    <div id="nrms-label-change">
                        <label>
                            NRMS Label :{" "}
                            <input
                                type="text"
                                value={nrmsAxisLabel}
                                onChange={handleNRMSLabelChange}
                            />
                        </label>
                    </div>
                </Box>
            </div>
            <Box
                id="width-change"
                style={{
                    marginLeft: "20px",
                    display: "flex",
                    flexDirection: "column",
                }}
            >
                Change Width
                <Slider
                    aria-label="Always visible"
                    value={graphWidth}
                    getAriaValueText={valuetext}
                    step={10}
                    max={2000}
                    marks={widthSliderMarks}
                    valueLabelDisplay="on"
                    onChange={handleWidthChange}
                    style={{ width: "80%" }}
                />
            </Box>
            <Box
                id="height-change"
                style={{
                    marginLeft: "20px",
                    display: "flex",
                    flexDirection: "column",
                }}
            >
                Change Height
                <Slider
                    aria-label="Always visible"
                    value={graphHeight}
                    getAriaValueText={valuetext}
                    step={10}
                    max={1000}
                    marks={heightSliderMarks}
                    valueLabelDisplay="on"
                    onChange={handleHeightChange}
                    style={{
                        width: "40%",
                    }}
                />
            </Box>
            <Box
                id="axis-range-change"
                style={{
                    marginLeft: "20px",
                    display: "flex",
                }}
            >
                <Box
                    id="height-change"
                    style={{
                        marginLeft: "20px",
                        display: "flex",
                        flexDirection: "column",
                    }}
                >
                    Change Depth Range
                    <Slider
                        getAriaLabel={() => "Depth range"}
                        value={depthAxisRange}
                        max={40}
                        onChange={handleDepthRangeChange}
                        marks={depthRangeMarks}
                        valueLabelDisplay="auto"
                        getAriaValueText={valuetext}
                    />
                </Box>
                <Box
                    id="height-change"
                    style={{
                        marginLeft: "50px",
                        display: "flex",
                        flexDirection: "column",
                    }}
                >
                    Change NRMS Range
                    <Slider
                        getAriaLabel={() => "NRMS range"}
                        value={nrmsAxisRange}
                        max={40}
                        onChange={handleNrmsRangeChange}
                        marks={nrmsRangeMarks}
                        valueLabelDisplay="auto"
                        getAriaValueText={valuetext}
                    />
                </Box>
            </Box>
        </>
    );
};

export default GraphSettings;
